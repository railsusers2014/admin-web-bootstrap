var $ = require('jquery');

var commonJSModuleInstance = require('app/modules/example-module-commonJS');
var AMDModuleInstance =require('app/modules/example-module-AMD');

commonJSModuleInstance.init();
AMDModuleInstance.init();

var loadElements = $('.js-condition').length;

if (loadElements) {
  console.log('condition was met');
  // elements is asynchronously (and conditionally) required here
  require.ensure(['app/ui/elements'], function(require) {
    var elements = require('app/ui/elements');

  });
} else {
  console.warning('condition was not met');
}
