var $ = require('jquery');

var exampleModule = {

	'init': function() {
  	console.log('commonJS Module init, $ is ' + $);
	}

};

module.exports = exampleModule; // expose this
